#!/bin/bash

set -e

for arg in "$@"
do
	case "${arg}" in
		--add-audio)
		if ! grep -qE '^dtoverlay=hifiberry-dac' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
			echo "Adding 'dtoverlay=hifiberry-dac' to config.txt (enables audio)."
			cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"
dtoverlay=hifiberry-dac
__EOF__
		fi
		;;
		--add-miniuart-bt-overlay)
		# upstream checks against dtoverlay= so it never adds this overlay with --add-audio enabled
		if ! grep -qE '^dtoverlay=miniuart-bt' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
			echo "Adding 'dtoverlay=miniuart-bt' to config.txt (fixes ttyAMA0 serial console)."
			cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# fixes rpi (3B, 3B+, 3A+, 4B and Zero W) ttyAMA0 serial console
dtoverlay=miniuart-bt
__EOF__
		fi
		;;
	esac

done
