# How to use

1. clone or download [buildroot](https://buildroot.org/download.html)
1. clone this repository
1. copy `rootfs/etc/wpa_supplicant.conf.dist` to `rootfs/etc/wpa_supplicant.conf` and adapt to your network
1. from buildroot directory, run:

```sh
make BR2_EXTERNAL=../rspeaker-system custom_pi0w_defconfig
```
